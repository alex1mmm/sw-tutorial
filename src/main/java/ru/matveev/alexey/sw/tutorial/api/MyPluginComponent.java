package ru.matveev.alexey.sw.tutorial.api;

public interface MyPluginComponent
{
    String getName();
}