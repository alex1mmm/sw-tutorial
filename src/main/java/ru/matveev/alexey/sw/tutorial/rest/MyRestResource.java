package ru.matveev.alexey.sw.tutorial.rest;

import com.atlassian.greenhopper.model.Epic;
import com.atlassian.greenhopper.service.Page;
import com.atlassian.greenhopper.service.PageRequests;
import com.atlassian.greenhopper.service.ServiceOutcome;
import com.atlassian.greenhopper.service.issue.RapidViewIssue;
import com.atlassian.greenhopper.service.issue.RapidViewIssueService;
import com.atlassian.greenhopper.service.issuelink.EpicService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * A resource of message.
 */
@Path("/message")
public class MyRestResource {

    private static final Logger LOG = LoggerFactory.getLogger(MyRestResource.class);
    private final EpicService epicService;
    private final IssueManager issueManager;
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final RapidViewIssueService rapidViewIssueService;

    @Inject
    public MyRestResource(@ComponentImport EpicService epicService,
                          @ComponentImport IssueManager issueManager,
                          @ComponentImport JiraAuthenticationContext jiraAuthenticationContext,
                          @ComponentImport RapidViewIssueService rapidViewIssueService) {
        this.epicService = epicService;
        this.issueManager = issueManager;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.rapidViewIssueService = rapidViewIssueService;
    }

    @Path("/hello")
    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response getMessage()
    {
       return Response.ok(new MyRestResourceModel("Hello World")).build();
    }

    @Path("/epictasks")
    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response getEpicTasks(@QueryParam("epic key") String epicKey)
    {
        ApplicationUser user = jiraAuthenticationContext.getLoggedInUser();
        ServiceOutcome<Epic> epic = epicService.getEpic(user, epicKey);
        Query query = JqlQueryBuilder.newBuilder().where().buildQuery();
        ServiceOutcome<Page<RapidViewIssue>> issues = rapidViewIssueService.getIssuesForEpic(user, epic.getValue(), PageRequests.request(0L, 100), query);
        List<String> issueList = issues.getValue().getValues().stream().map(el -> el.getIssue().getSummary()).collect(Collectors.toList());
        return Response.ok(new MyRestResourceModel(issueList.toString())).build();
    }

    @Path("/epictasks")
    @POST
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response addTaskToEpic(@QueryParam("epic key") String epicKey,
                                  @QueryParam("issue key") String issueKey)
    {
        ApplicationUser user = jiraAuthenticationContext.getLoggedInUser();
        ServiceOutcome<Epic> epic = epicService.getEpic(user, epicKey);
        Set<Issue> issueSet = new HashSet<>();
        issueSet.add(issueManager.getIssueByCurrentKey(issueKey));
        epicService.addIssuesToEpic(user, epic.getValue(), issueSet);
        return Response.ok(new MyRestResourceModel(issueKey + " added to " + epicKey)).build();
    }
}