package ut.ru.matveev.alexey.sw.tutorial;

import org.junit.Test;
import ru.matveev.alexey.sw.tutorial.api.MyPluginComponent;
import ru.matveev.alexey.sw.tutorial.impl.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}